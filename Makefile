swag:
	swag init -g internal/app/app.go
build:
	go mod download && go build -o ./.bin/app.exe ./cmd/app/main.go