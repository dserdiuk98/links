package repository

import (
	"context"
	"errors"
	"gitlab.com/dserdiuk98/links/internal/models"
	"sync"
)

type LinkLocalStorage struct {
	links map[string]*models.Link
	mutex *sync.RWMutex
}

func NewLinkLocalstorage() *LinkLocalStorage {
	return &LinkLocalStorage{
		links: make(map[string]*models.Link),
		mutex: new(sync.RWMutex),
	}
}

func (r *LinkLocalStorage) Create(ctx context.Context, link *models.Link) (*models.Link, error) {
	r.mutex.Lock()
	defer r.mutex.Unlock()
	r.links[link.Hash] = link
	return link, nil
}

func (r *LinkLocalStorage) Exists(ctx context.Context, hash string) bool {
	r.mutex.Lock()
	defer r.mutex.Unlock()
	for _, link := range r.links {
		if link.Hash == hash {
			return true
		}
	}
	return false
}

func (r *LinkLocalStorage) List(ctx context.Context) ([]models.Link, error) {
	r.mutex.RLock()
	defer r.mutex.RUnlock()
	links := make([]models.Link, len(r.links))
	for _, link := range r.links {
		links = append(links, *link)
	}
	return links, nil
}

func (r *LinkLocalStorage) Get(ctx context.Context, hash string) (*models.Link, error) {
	r.mutex.RLock()
	defer r.mutex.RUnlock()
	link, ok := r.links[hash]
	if !ok {
		return nil, errors.New("Link not found")
	}
	return link, nil
}
