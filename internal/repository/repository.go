package repository

import (
	"context"
	"gitlab.com/dserdiuk98/links/internal/models"
)

type Link interface {
	Create(ctx context.Context, link *models.Link) (*models.Link, error)
	Exists(ctx context.Context, hash string) bool
	Get(ctx context.Context, hash string) (*models.Link, error)
	List(ctx context.Context) ([]models.Link, error)
}
