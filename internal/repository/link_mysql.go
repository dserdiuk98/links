package repository

import (
	"context"
	"database/sql"
	"gitlab.com/dserdiuk98/links/internal/models"
)

type MysqlStorage struct {
	Con *sql.DB
}

func NewMysqlStorage(con *sql.DB) *MysqlStorage {
	return &MysqlStorage{Con: con}
}

func (r *MysqlStorage) Create(ctx context.Context, link *models.Link) (*models.Link, error) {
	_, err := r.Con.Query("INSERT INTO links (`hash`, `url`) VALUES (?, ?)", link.Hash, link.Url)
	return link, err
}

func (r *MysqlStorage) Get(ctx context.Context, hash string) (*models.Link, error) {
	var link models.Link
	err := r.Con.QueryRow("SELECT `hash`, `url` FROM `links` WHERE `hash` = ?", hash).Scan(&link.Hash, &link.Url)
	if err != nil {
		return nil, err
	}
	return &link, nil
}

func (r *MysqlStorage) List(ctx context.Context) ([]models.Link, error) {
	links := make([]models.Link, 0)
	rows, err := r.Con.Query("SELECT `hash`, `url` FROM `links`")

	if err != nil {
		return links, err
	}
	defer rows.Close()
	for rows.Next() {
		link := new(models.Link)
		err = rows.Scan(&link.Hash, &link.Url)
		if err != nil {
			return links, err
		}
		links = append(links, *link)
	}
	return links, nil
}
