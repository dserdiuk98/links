package http

import (
	"github.com/gin-gonic/gin"
	"github.com/swaggo/files"
	"github.com/swaggo/gin-swagger" // gin-swagger middleware
	_ "gitlab.com/dserdiuk98/links/docs"
	"gitlab.com/dserdiuk98/links/internal/config"
	"gitlab.com/dserdiuk98/links/internal/delivery/http/input"
	"gitlab.com/dserdiuk98/links/internal/delivery/http/response"
	"gitlab.com/dserdiuk98/links/internal/service"
	"net/http"
)

type Handler struct {
	services *service.Services
	cfg      *config.Conf
}

func NewHandler(services *service.Services, cfg *config.Conf) *Handler {
	return &Handler{services: services, cfg: cfg}
}

func (h Handler) Init() *gin.Engine {
	router := gin.Default()
	router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	api := router.Group("api")
	router.NoRoute(func(context *gin.Context) {
		context.JSON(404, gin.H{"code": "PAGE_NOT_FOUND", "message": "Page not found"})
	})
	h.configureApiRouter(api)
	router.GET("/l/:hash", h.redirectByHash)
	router.Static("/web", "./web")
	return router
}

func (h Handler) redirectByHash(context *gin.Context) {
	hash := context.Param("hash")

	link, err := h.services.Link.GetLink(context, hash)
	if err != nil {
		context.AbortWithError(http.StatusNotFound, err)
		return
	}
	context.Redirect(http.StatusMovedPermanently, link.Url)
	context.Abort()
}

func (h Handler) configureApiRouter(r *gin.RouterGroup) {
	group := r.Group("links")
	group.GET("list", h.getLinks)
	group.POST("create", h.createLink)
}

// @Summary Get list of links
// @Description Get details links
// @Tags links
// @Accept  json
// @Produce  json
// @Success 200 {array} models.Link
// @Router /links/list [get]
func (h Handler) getLinks(context *gin.Context) {
	links, err := h.services.Link.ListLinks(context)
	if err != nil {
		newResponse(context, http.StatusInternalServerError, err.Error())
		return
	}
	newResponse(context, http.StatusOK, links)
}

// @Summary Create Short Link
// @Accept  json
// @Produce  json
// @Param input body input.CreateLink true "Url"
// @Success 201 {object} response.CreateLink
// @Failure 400,404 {object} resp
// @Failure default {object} resp
// @Router /links/create [post]
func (h Handler) createLink(context *gin.Context) {
	var data input.CreateLink
	if err := context.BindJSON(&data); err != nil {
		newResponse(context, http.StatusBadRequest, "invalid body input")
		return
	}

	link, err := h.services.Link.CreateLink(context, data.Url)
	if err != nil {
		newResponse(context, http.StatusInternalServerError, err.Error())
		return
	}

	newResponse(context, http.StatusCreated, response.CreateLink{Url: "http://localhost:8080/l/" + link.Hash})
}
