package input

type CreateLink struct {
	Url string `json:"url" binding:"required"`
}
