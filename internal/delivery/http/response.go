package http

import (
	"github.com/gin-gonic/gin"
)

type resp struct {
	Message interface{} `json:"message"`
}

func newResponse(c *gin.Context, statusCode int, message interface{}) {
	c.AbortWithStatusJSON(statusCode, resp{message})
}
