package service

import (
	"context"
	"errors"
	"testing"

	"github.com/golang/mock/gomock"
	"gitlab.com/dserdiuk98/links/internal/models"
	"gitlab.com/dserdiuk98/links/internal/repository"
	mock_repository "gitlab.com/dserdiuk98/links/internal/repository/mocks"
	"gitlab.com/dserdiuk98/links/pkg/hash"
	mock_hash "gitlab.com/dserdiuk98/links/pkg/hash/mocks"
	"gopkg.in/go-playground/assert.v1"
)

func TestCreateLink(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	ctx := context.Background()
	res := models.Link{
		Url:  "aaaa",
		Hash: "bbbb",
	}

	tt := []struct {
		name     string
		url      string
		result   *models.Link
		mockHash func() hash.Encryption
		mockRepo func() repository.Link
	}{
		// case 1
		{
			name:   "hash error",
			url:    "aaaa",
			result: nil,
			mockHash: func() hash.Encryption {
				m := mock_hash.NewMockEncryption(mockCtrl)
				m.EXPECT().Encrypt("aaaa").Return("", errors.New("Empty response"))
				return m
			},
			mockRepo: func() repository.Link {
				// в первом кейсе до репы не дойдёт, так как ошибка на hash
				return nil
			},
		},
		// case 2
		{
			name:   "success",
			url:    "aaaa",
			result: &res,
			mockHash: func() hash.Encryption {
				m := mock_hash.NewMockEncryption(mockCtrl)
				m.EXPECT().Encrypt("aaaa").Return("bbbb", nil)
				return m
			},
			mockRepo: func() repository.Link {
				m := mock_repository.NewMockLink(mockCtrl)
				m.EXPECT().Create(ctx, &res).Return(&res)
				return m
			},
		},
	}

	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			s := NewLinkService(tc.mockRepo(), tc.mockHash())
			r := s.CreateLink(ctx, tc.url)
			if tc.result == nil {
				assert.Equal(t, r, nil)
				return
			}
			assert.NotEqual(t, r, nil)
			assert.Equal(t, r.Url, tc.result.Url)
			assert.Equal(t, r.Hash, tc.result.Hash)
		})
	}
}
