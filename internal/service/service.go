package service

import (
	"context"
	"gitlab.com/dserdiuk98/links/internal/models"
)

type Services struct {
	Link Link
}

func NewServices(linkService Link) *Services {
	return &Services{Link: linkService}
}

//go:generate mockgen -source=service.go -destination=mocks/mock.go
type Link interface {
	CreateLink(ctx context.Context, url string) (*models.Link, error)
	GetLink(ctx context.Context, hash string) (*models.Link, error)
	ListLinks(ctx context.Context) ([]models.Link, error)
}
