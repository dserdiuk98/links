package service

import (
	"context"
	"gitlab.com/dserdiuk98/links/internal/models"
	"gitlab.com/dserdiuk98/links/internal/repository"
	"gitlab.com/dserdiuk98/links/pkg/hash"
)

type LinkService struct {
	repo repository.Link
	hash hash.Encryption
}

func NewLinkService(r repository.Link, encrypt hash.Encryption) *LinkService {
	return &LinkService{repo: r, hash: encrypt}
}

func (c LinkService) CreateLink(ctx context.Context, url string) (*models.Link, error) {
	hashExists := true
	var urlHash string
	var err error
	for hashExists {
		urlHash, err = c.hash.Encrypt(url)

		if err != nil {
			return nil, err
		}
		hashExists = c.repo.Exists(ctx, urlHash)
	}

	l := &models.Link{
		Hash: urlHash,
		Url:  url,
	}

	return c.repo.Create(ctx, l)
}

func (c LinkService) GetLink(ctx context.Context, hash string) (*models.Link, error) {
	return c.repo.Get(ctx, hash)
}

func (c LinkService) ListLinks(ctx context.Context) ([]models.Link, error) {
	return c.repo.List(ctx)
}
