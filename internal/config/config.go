package config

import (
	"github.com/joho/godotenv"
	"os"
)

type Http struct {
	Port string
}

type Conf struct {
	Http Http
}

func Get() *Conf {
	err := godotenv.Load("./config/app.env")
	if err != nil {
		panic(err)
	}
	config := Conf{
		Http: Http{
			Port: os.Getenv("HTTP_PORT"),
		},
	}

	return &config
}
