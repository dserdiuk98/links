package server

import (
	"context"
	"gitlab.com/dserdiuk98/links/internal/config"
	"net/http"
)

type Server struct {
	httpServer *http.Server
}

func NewServer(handler http.Handler, cfg *config.Conf) *Server {
	return &Server{httpServer: &http.Server{
		Addr:    ":" + cfg.Http.Port,
		Handler: handler,
	}}
}

func (s Server) Run() error {
	return s.httpServer.ListenAndServe()
}

func (s *Server) Stop(ctx context.Context) error {
	return s.httpServer.Shutdown(ctx)
}
