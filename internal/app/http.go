package app

import (
	"context"
	"gitlab.com/dserdiuk98/links/internal/config"
	"gitlab.com/dserdiuk98/links/internal/delivery/http"
	"gitlab.com/dserdiuk98/links/internal/repository"
	"gitlab.com/dserdiuk98/links/internal/server"
	"gitlab.com/dserdiuk98/links/internal/service"
	"gitlab.com/dserdiuk98/links/pkg/hash"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"
)

// @title Link APP
// @version 1.0
// @description test swagger

// @host localhost:8080
// @BasePath /api/

func RunHttp() {
	cfg := config.Get()
	linkRepo := repository.NewLinkLocalstorage()
	//encryption := hash.NewFnvHashEncryption()
	//linkRepo := repository.NewMysqlStorage(mysql.NewConnection(""))
	//encryption := hash.NewUUIDEncryption()
	encryption := hash.NewRandomStringEncryption(7)
	services := service.NewServices(service.NewLinkService(linkRepo, encryption))
	h := http.NewHandler(services, cfg)

	srv := server.NewServer(h.Init(), cfg)

	go func() {
		if err := srv.Run(); err != nil {
			log.Fatalf(err.Error())
		}
	}()

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit
	log.Println("Shutting down server...")

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := srv.Stop(ctx); err != nil {
		log.Fatal("Server forced to shutdown:", err)
	}

	log.Println("Server exiting")
}
