package app

import (
	"context"
	"gitlab.com/dserdiuk98/links/internal/delivery/rpc"
	"google.golang.org/grpc"
	"google.golang.org/grpc/grpclog"
	"net"
)

func RunRPC() {
	listener, err := net.Listen("tcp", ":5300")

	if err != nil {
		grpclog.Fatalf("failed to listen: %v", err)
	}

	var opts []grpc.ServerOption
	grpcServer := grpc.NewServer(opts...)

	rpc.RegisterLinksServer(grpcServer, &serverRPC{})
	grpcServer.Serve(listener)
}

type serverRPC struct {
	rpc.UnimplementedLinksServer
}

func (s serverRPC) Create(ctx context.Context, request *rpc.CreateRequest) (*rpc.CreateResponse, error) {
	response := &rpc.CreateResponse{
		Url: "xer",
	}
	return response, nil
}
