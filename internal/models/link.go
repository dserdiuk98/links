package models

type Link struct {
	Hash string `json:"hash"`
	Url  string `json:"url"`
}