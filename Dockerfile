FROM golang:alpine as builder
WORKDIR /app
COPY . .
RUN go build -o links ./cmd/app

FROM alpine:latest
WORKDIR /root/
COPY --from=builder /app/links .
COPY ./config ./config
EXPOSE 8080
CMD ["./links"]