package mysql

import (
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
)

func NewConnection(connectionString string) *sql.DB {
	db, err := sql.Open("mysql", "root:root@tcp(127.0.0.1:3307)/links")

	if err != nil {
		panic(err.Error())
	}

	return db
}
