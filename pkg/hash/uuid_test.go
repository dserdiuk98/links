package hash

import (
	"gopkg.in/go-playground/assert.v1"
	"testing"
)

func TestUUIDEncryption(t *testing.T) {
	encryption := NewUUIDEncryption()
	hash, err := encryption.Encrypt("https://google.com")

	if err != nil {
		t.Error(err)
	}

	assert.NotEqual(t, hash, "")
}
