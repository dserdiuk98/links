package hash

import "github.com/google/uuid"

type UUIDHash struct{}

func NewUUIDEncryption() *UUIDHash {
	return &UUIDHash{}
}

func (h UUIDHash) Encrypt(url string) (string, error) {
	hash := uuid.New()

	return hash.String(), nil
}
