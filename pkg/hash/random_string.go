package hash

import "math/rand"

type RandomString struct {
	StringLength int
}

func NewRandomStringEncryption(stringLength int) *RandomString {
	return &RandomString{
		StringLength: stringLength,
	}
}

var letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

func (s RandomString) Encrypt(url string) (string, error) {
	b := make([]rune, s.StringLength)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b), nil
}
