package hash

import (
	"fmt"
	"hash/fnv"
)

type FnvHash struct {}

func NewFnvHashEncryption() *FnvHash {
	return &FnvHash{}
}

func (f FnvHash) Encrypt(url string) (string, error) {
	h := fnv.New32a()
	_, err := h.Write([]byte(url))
	if err != nil {
		return "", err
	}
	return fmt.Sprint(h.Sum32()), nil
}