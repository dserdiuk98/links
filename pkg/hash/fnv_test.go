package hash

import (
	"gopkg.in/go-playground/assert.v1"
	"testing"
)

func TestFnvEncryption(t *testing.T) {
	testCases := []struct {
		name   string
		url    string
		result string
	}{
		{
			name:   "success",
			url:    "https://vk.com",
			result: "169647742",
		},
	}

	fnvEncryption := NewFnvHashEncryption()

	for _, testCase := range testCases {
		res, err := fnvEncryption.Encrypt(testCase.url)
		if err != nil {
			t.Error(err)
		}
		assert.Equal(t, testCase.result, res)
	}
}
