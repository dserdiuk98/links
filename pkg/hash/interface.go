package hash

type Encryption interface {
	Encrypt (url string) (string, error)
}